<?php
/**
 * @file
 * gk_table_booking.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gk_table_booking_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location_table_booking|node|location|form';
  $field_group->group_name = 'group_location_table_booking';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Table booking',
    'weight' => '14',
    'children' => array(
      0 => 'field_location_table_booking_ref',
      1 => 'field_location_table_booking_sup',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-location-table-booking field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_location_table_booking|node|location|form'] = $field_group;

  return $export;
}
