<?php

$plugin = array(
  'title' => 'Table booking: Widget',
  'category' => 'GK Table booking',
  'single' => TRUE,
);

function gk_table_booking_gk_table_booking_widget_content_type_render($subtype, $conf, $args, $context) {
  if ($current_location = gk_locations_get_current_location()) {
    if ($info = gk_table_booking_get_info_from_location($current_location)) {
      if (!empty($info['supplier']) && !empty($info['reference'])) {
        $theme = 'gk_table_booking_' . $info['supplier']['name'] . '_widget';

        return (object) array(
          'title' => '',
          'content' => array(
            '#theme' => $theme,
            '#supplier' => $info['supplier'],
            '#reference' => $info['reference'],
          ),
        );
      }
    }
  }
}

function gk_table_booking_gk_table_booking_widget_content_type_edit_form($form, &$form_state) {
  return $form;
}

function gk_table_booking_gk_table_booking_widget_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
