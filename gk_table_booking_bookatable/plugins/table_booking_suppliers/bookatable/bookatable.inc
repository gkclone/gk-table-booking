<?php

/**
 * Preprocess function for the gk_table_booking_bookatable_widget theme.
 */
function gk_table_booking_bookatable_preprocess_gk_table_booking_bookatable_widget(&$variables) {
  $config = array(
    'connectionid' => $variables['reference'],
  );

  if ($ga_account_number = variable_get('googleanalytics_account')) {
    $config['gaAccountNumber'] = $ga_account_number;
  }

  $variables['embed_config'] = drupal_json_encode($config);
}
