<?php
/**
 * @file
 * gk_table_booking.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_table_booking_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
